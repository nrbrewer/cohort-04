import pytest
from fizzbuzz import fizzbuzz


def test_fizzbuzz_three_returns_fizz():
    # Setup
    number = 3
    # Execute the code under test
    result = fizzbuzz(number)
    # Examine results and assert
    assert 'fizz' == result

def test_fizzbuzz_five_returns_buzz():
    '''Test that five returns the value "fizz"'''
    number = 5
    result = fizzbuzz(number)
    assert 'buzz' == result

def test_fizzbuzz_one_returns_one():
    number = 1
    result = fizzbuzz(number)
    assert 1 == result

def test_fizzbuzz_two_returns_two():
    number = 2
    result = fizzbuzz(number)
    assert 2 == result

def test_fizzbuzz_six_returns_fizz():
    number = 6
    result = fizzbuzz(number)
    assert 'fizz' == result

def test_fizzbuzz_10_returns_buzz():
    number = 10 
    result = fizzbuzz(number)
    assert 'buzz' == result

def test_fizzbuzz_15_returns_fizzbuzz():
    number = 15
    result = fizzbuzz(number)
    assert 'fizzbuzz' == result

def test_fizzbuzz_0_raises_ValueError():
    number = 0
    with pytest.raises(ValueError):
        result = fizzbuzz(number)

def test_fizzbuzz_101_raises_ValueError():
    number = 101
    with pytest.raises(ValueError):
        result = fizzbuzz(number)

def test_fizzbuzz_102_raises_ValueError():
    number = 102
    with pytest.raises(ValueError):
        result = fizzbuzz(number)

def test_fizzbuzz_negative_raises_ValueError():
    number = -1
    with pytest.raises(ValueError):
        result = fizzbuzz(number)

def test_fizzbuzz_string_raises_TypeError():
    value = 'Hello!'
    with pytest.raises(TypeError):
        result = fizzbuzz(value)
