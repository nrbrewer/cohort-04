import pytest
from weather import get_location


class MockResponse:
    '''Class to fake out the actual Response class that requests returns.
       Supports a json() function that returns a dictionary, and a status code
       (so you can test for error statuses more easily)'''
    def __init__(self, json_data, status_code):
        '''Accepts a dict of data to mimic the API response, and a status code
        value'''
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        '''Just returns whatever was passed in in __init__'''
        return self.json_data


def mock_get_location_success(*args, **kwargs):
    print('Calling mock requests.get')
    data = { 
        "status":"success",
        "data":{
            "ipv4":"52.2.13.75",
            "continent_name":"North America",
            "country_name":"United States",
            "subdivision_1_name":"Virginia",
            "subdivision_2_name": None,
            "city_name":"Ashburn",
            "latitude":"39.04810",
            "longitude":"-77.47280"}}
    response = MockResponse(
        json_data=data,
        status_code=200)
    return response

def mock_get_location_404(*args, **kwargs):
    data = {}
    response = MockResponse(
        json_data=data,
        status_code=404)
    return response


def test_get_location_returns_values(mocker):
    mocker.patch('requests.get', mock_get_location_success)
    lat, lon = get_location()
    assert lat is not None
    assert lon is not None

def test_get_location_404_error(mocker):
    mocker.patch('requests.get', mock_get_location_404)
    lat, lon = get_location()
    assert lat is None
    assert lon is None

def test_get_location_500_error(mocker):
    data = {}
    response = MockResponse(
        json_data=data,
        status_code=500)
    mock_get = mocker.patch('requests.get')
    mock_get.return_value = response

    lat, lon = get_location()
    assert lat is None
    assert lon is None
