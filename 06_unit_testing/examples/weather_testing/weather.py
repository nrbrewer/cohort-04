#! /usr/bin/env python3
import requests

DARK_SKY_SECRET_KEY="d21b17405d692b8977dd9098c59754eb"
LOCATION_ENDPOINT = 'https://ipvigilante.com/'

def get_location():
    """ Returns the longitude and latitude for the location of this machine.

    Returns:
    str: longitude
    str: latitude
    """
    response = requests.get(LOCATION_ENDPOINT)
    if response.status_code == 404 or response.status_code == 500:
        return None, None
    data = response.json()
    lat = data['data']['latitude']
    lon = data['data']['longitude']

    return lat, lon

if __name__ == '__main__':
    print(get_location())
